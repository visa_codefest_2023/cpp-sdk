import requests
import sys
import json
import base64
import os
 
data = json.loads(json.dumps(Hook['params']))
currentbranch = data["repository"]["name"]
reciverbranch = []
branches = ['cs-sdk', 'java-sdk', 'cpp-sdk', 'angular-sdk']

print('i am in log')

def jira_rest_call(sData):
 
  postURL = 'https://m2m-sdk-pallav.atlassian.net/rest/api/2/issue'
  post_resp=requests.post(url=postURL,data=sData,headers = {'content-type': 'application/json'},auth=('pallavdgr8@gmail.com', 'DXYdcJcgyDwocSV8p4ZP343E'),verify=True)
  print(post_resp.content)
  # Send the request and grab JSON response
  # Load into a JSON object and return that to the calling function
  return json.loads(post_resp.content)

for b in branches:
    if b != currentbranch:
        reciverbranch.append(b)
print(reciverbranch)


# Build the text for the JIRA ticket.
Jira_label_From = 'SDK_SyncUp_From_' + currentbranch

for r in reciverbranch:
    Jira_label_To = 'SDK_SyncUp_To_' + r
    jira_summary = '[' + Jira_label_From +'] : [' + Jira_label_To +'] '+ data["push"]["changes"][0]["commits"][0]["message"] 
    jira_description = 'This is an automatically created SDK syncup ticket because of commit in ' + currentbranch + ' repoistory for the owners of sdk repository ' + r + r' . The idea these auto generated tickets is to make sure that all the various SDKs supported in various languages by cumulocity have addressed the new change. This new change could be because of following reasons - \\\\ - Change in the cumulocity core API definition or internals demanding change in the way apis need to be called. \\\\ - Bug found in the implementaion of any SDK and it is highly probable that other implementaions also have the same bug. \\\\ \\\\ The SDK Owner needs to address this ticket to make sure all the SDKs are upto date and there are as less differences in SDKs as possible. SDK owner(s) need to do the needful based on mentioned details of the commit which triggred creation this ticket. Below are the details - \\\\ \\\\ Commit link - ' + data["push"]["changes"][0]["links"]["html"]["href"] + r' \\\\ \\\\ Commit patch diff  - ' + data["push"]["changes"][0]["links"]["diff"]["href"] + r' \\\\ \\\\ Commit hash ID - ' + data["push"]["changes"][0]["old"]["target"]["hash"] + r' \\\\ \\\\ Commit Owner - ' + data["push"]["changes"][0]["old"]["target"]["author"]["raw"] 
    # Build the JSON to post to JIRA
    json_data = '''
    {
        "fields":{
            "project":{
                "key":"M2M"
            },
            "summary": "%s",
            
            "issuetype":{
                "name":"Bug"
            },
            "description": "%s",
             "labels": [
     "%s",
     "%s"
     ]
        } 
    } ''' % (jira_summary, jira_description , Jira_label_From , Jira_label_To)

    print(json_data)

    json_response = jira_rest_call(json_data)
parent_key = json_response['key']

print ("Created parent issue ", parent_key)